#include <stdio.h>
#include <string.h>
#include <unistd.h>
int main()
{
	pid_t pip;
	char buffer[20];
	memset(buffer,0,strlen(buffer));
	pip = fork();
	if(pip == 0)
	{
		close(1);
		//ll print only one "hello world"
		//cause the child close the output
	}
		printf("hello world\n");
}
